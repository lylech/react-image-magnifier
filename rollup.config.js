import { uglify } from 'rollup-plugin-uglify'
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import babel from "@rollup/plugin-babel";
import postcss from "rollup-plugin-postcss";

const NODE_ENV = process.env.NODE_ENV || "development";
export default {
  input: "./src/index.js",
  output: {
    file: "./dist/index.js",
    format: "es"
  },
  plugins: [
    postcss({
      plugins: []
    }),
    babel({
      exclude: "node_modules/**",
      presets: ["@babel/preset-env", "@babel/preset-react"],
      babelHelpers: 'bundled'
    }),
    resolve(),
    commonjs()/
    uglify(),
  ],
  external: ['react', 'react-dom'],
};
