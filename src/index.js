import LylechImageMagnifierJS from './lfc-magnifier.js';
import defaultCss from './lfc-magnifier.css';

export { LylechImageMagnifierJS as RIMagnifier, defaultCss as RIMDefaultCss };
