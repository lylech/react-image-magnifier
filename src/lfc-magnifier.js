export default class LylechImageMagnifierJS{

    constructor(args){
        if (!args.container){
            console.error("container must be specfied");
            return;
        }
        if (args.container.getAttribute("inuse")){
            console.warn("Attempt to reinitialize class prevented!");
            window.location.reload();
        }
        this.arrayOfImagePaths = args.arrayOfImagePaths || [];
        this.arrayOfImageZoomFactors = args.arrayOfImageZoomFactors || [];
        for (let i=this.arrayOfImageZoomFactors.length; i<this.arrayOfImagePaths.length; i++){
            this.arrayOfImageZoomFactors.push(2);
        }
        new Array(this.arrayOfImagePaths.length).fill(2);
        this.fnClickHandler = args.fnClickHandler;
        this.initialSelection = args.initialSelection || 0;

        this.container = args.container;
        this.container.setAttribute("inuse", "true");

        let [oProduct, oThumbnails, oProductImg] = this.createScaffolding(this.container);
        this.oProduct = oProduct;
        this.oProductImg = oProductImg;

        window.addEventListener("resize", (e)=>{
            clearTimeout(this.resizeListener);
            this.resizeListener = setTimeout(()=>fnResizeListener(e), 100);
        });

        const fnResizeListener = (e)=>{
            this.thumbclick(this.arrayOfImagePaths[this.selectedIdxImage], oProductImg, oProduct, this.selectedIdxImage, false);
        }

        const touchmoveListener = (e)=>{
            e.preventDefault();
            const ee = [];
            ee.pageX = e.touches[0].pageX;
            ee.pageY = e.touches[0].pageY;
            hoverListener(ee);
        }

        const hoverListener = (e)=>{

            let bspaceX = -(e.pageX-oProduct.offsetLeft-oProduct.clientWidth/2)/oProduct.clientWidth*100;
            let bspaceY = -(e.pageY-oProduct.offsetTop-oProduct.clientHeight/2)/oProduct.clientHeight*100;

            if (bspaceX<-25) bspaceX = -25; else if (bspaceX>25) bspaceX = 25;
            if (bspaceY<-25) bspaceY = -25; else if (bspaceY>25) bspaceY = 25;

            let imageRangeX = oProductImg.width-oProduct.clientWidth;
            let imageRangeY = oProductImg.height-oProduct.clientHeight;

            if (imageRangeX<0) imageRangeX=0;
            if (imageRangeY<0) imageRangeY=0;

            let imageX = ((bspaceX + 25) * 2)/100 * imageRangeX;
            let imageY = ((bspaceY + 25) * 2)/100 * imageRangeY;

            let xlatX = (oProductImg.width - oProduct.clientWidth) / 2;
            let xlatY = (oProductImg.height - oProduct.clientHeight) / 2;

            if (xlatX<0) xlatX=0;
            if (xlatY<0) xlatY=0;

            imageX = imageX - xlatX;
            imageY = imageY - xlatY;

            oProductImg.style.left = imageX + "px";
            oProductImg.style.top = imageY + "px";
            return;
        };

        const wheelListener = (e)=>{
            //console.log("wheelEvent: %o",e );
            if (e.wheelDelta>0){
                //this.arrayOfImageZoomFactors[this.selectedIdxImage] = this.arrayOfImageZoomFactors[this.selectedIdxImage] * 1.1;
            } else {
                //this.arrayOfImageZoomFactors[this.selectedIdxImage] = this.arrayOfImageZoomFactors[this.selectedIdxImage] / 1.1;
            }
        }

        oProduct.addEventListener("mouseenter", (e)=>{
            const zoomFactor = this.arrayOfImageZoomFactors[this.selectedIdxImage];
            oProductImg.bag = {width: oProductImg.style.width, height: oProductImg.style.height};
            oProductImg.style.width = oProductImg.width * zoomFactor + "px";
            oProductImg.style.height = oProductImg.height * zoomFactor + "px";
            oProduct.addEventListener("mousemove", hoverListener);
            oProduct.addEventListener("touchmove", touchmoveListener);
            oProduct.addEventListener("wheel", wheelListener);
        });

        oProduct.addEventListener("mouseleave", (e)=>{
            oProduct.removeEventListener("mousemove", hoverListener);
            oProduct.removeEventListener("touchmove", touchmoveListener);
            oProduct.removeEventListener("wheel", wheelListener);

            oProductImg.style.width = oProductImg.bag["width"];
            oProductImg.style.height = oProductImg.bag["height"];
            oProductImg.style.left = 0;
            oProductImg.style.top = 0;
        });

        this.arrayOfThumbnailContainers = this.createThumbnails(this.arrayOfImagePaths, oThumbnails, oProductImg, oProduct, this.thumbclick);
        this.thumbclick(this.arrayOfImagePaths[this.initialSelection], oProductImg, oProduct, this.initialSelection, true);
    }

    nextImage = ()=>{
        const idxNextImage = (this.selectedIdxImage + 1) % this.arrayOfImagePaths.length;
        this.thumbclick(this.arrayOfImagePaths[idxNextImage], this.oProductImg, this.oProduct, idxNextImage, true);
    }

    previousImage = ()=>{
        let idxNextImage = ((this.selectedIdxImage - 1) + this.arrayOfImagePaths.length) % this.arrayOfImagePaths.length;
        // if (idxNextImage<0) idxNextImage=this.arrayOfImagePaths.length-1;
        this.thumbclick(this.arrayOfImagePaths[idxNextImage], this.oProductImg, this.oProduct, idxNextImage, true);
    }

    setCurrentImage = (idxImage, doTriggerAnimation)=>{
        this.selectedIdxImage = idxImage;
        if (doTriggerAnimation && this.fnClickHandler) this.fnClickHandler(idxImage);

        this.arrayOfThumbnailContainers[idxImage].classList.add("active");
        if (typeof this.idxPreviousImage !== 'undefined' && this.idxPreviousImage !== idxImage) this.arrayOfThumbnailContainers[this.idxPreviousImage].classList.remove("active");
        this.idxPreviousImage = idxImage;
    }

    thumbclick = (strImg, oProductImage, oProductImageContainer, idxImage, doTriggerAnimation) => {
        if (doTriggerAnimation && this.idxPreviousImage===idxImage) return;
        oProductImage.style.width="0";
        oProductImage.style.height="0";
        const idxPreviousImage = this.idxPreviousImage;
        oProductImage.onload = ()=>{
            let containerWidth = oProductImageContainer.scrollWidth;
            let containerHeight = oProductImageContainer.scrollHeight;

            let nw = oProductImage.naturalWidth;
            let nh = oProductImage.naturalHeight;

            let imgAspectRatio = nw/nh;
            let containerAspectRatio = containerWidth/containerHeight;
            if (imgAspectRatio > containerAspectRatio){
                oProductImage.style.width = containerWidth + "px";
                oProductImage.style.height = parseInt(nh * containerWidth / nw) + "px";

            } else {
                oProductImage.style.width = parseInt(nw * containerHeight / nh) + "px";
                oProductImage.style.height = containerHeight + "px";
            }

            if (doTriggerAnimation && (idxPreviousImage !== idxImage)){
                oProductImageContainer.classList.remove("selected");
                void oProductImageContainer.offsetWidth;
                oProductImageContainer.classList.add("selected");
            }
        }
        oProductImage.setAttribute("src", strImg);
        this.setCurrentImage(idxImage, doTriggerAnimation);
    };

    createThumbnails = (
        arrayOfImagePaths, 	//string: path to image files for thumbnails
        oThumbnails,		//DIV	: container to which to add thumbnail
        oProductImg,		//IMG	: Product display: img
        oProductImgContainer, //DIV : Product display: container for img
        fnClick,			//Function: Callback for thumbnail click event handler
    )=>{
        const arrayOfThumbnailContainers = [];
        arrayOfImagePaths.forEach( (strImg, idxImage) => {
            let oSpan = document.createElement("span");
            oSpan.className = "lfc-rim-thumbnail-outer";
            oSpan.style.setProperty('--i', idxImage);

            let oDiv = document.createElement('div');
            oDiv.className = "lfc-rim-thumbnail";
            oDiv.style.backgroundImage="url('"+strImg+"')";
            oDiv.style.setProperty('--i', idxImage);
            oDiv.addEventListener('click', (e) => { fnClick(strImg, oProductImg, oProductImgContainer, idxImage, true) });

            // oThumbnails.appendChild(oDiv);
            // arrayOfThumbnailContainers.push(oDiv);
            oSpan.appendChild(oDiv);
            oThumbnails.appendChild(oSpan);
            arrayOfThumbnailContainers.push(oSpan);
        });
        return arrayOfThumbnailContainers;
    };

    createScaffolding(container){
        let oDivLeft = document.createElement('div');
        oDivLeft.className = "lfc-rim-main";

        let oDivProduct = document.createElement('div');
        oDivProduct.className = "lfc-rim-product";

        let oImgProduct = document.createElement('img');
        oImgProduct.className = "lfc-rim-product-img";

        let oDivThumbnails = document.createElement('div');
        oDivThumbnails.className = "lfc-rim-thumbnails";

        oDivProduct.appendChild(oImgProduct);
        oDivLeft.appendChild(oDivProduct);
        oDivLeft.appendChild(oDivThumbnails);
        container.appendChild(oDivLeft);

        return [oDivProduct, oDivThumbnails, oImgProduct];
    }
};
